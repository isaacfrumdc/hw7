//JHUgle.java

package hw7;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Stack;

/**
 * Use the HashMap.
 * */
public final class JHUgle {

    private static Stack<String> s;
    private static HashMap<String, HashMap<String, Integer>> hash;

    private JHUgle() {}

    /** Main for JHUgle.
     * @param args the file to open.
     * @throws FileNotFoundException thrown if file does not exist.
     */
    public static void main(String[] args) throws FileNotFoundException {

        if (args[0] == null) {
            System.err.println("No file entered");
        } else {
            String filename = args[0];
            File file = new File(filename);

            Scanner fs = new Scanner(file);

            hash = new HashMap<>();

            //READ IN FILE AND CONSTRUCT INDEX

            while (fs.hasNextLine()) {
                loadHashMap(fs);
            }
            fs.close();

            System.out.println("Index Created");

            //OPERATIONS

            Scanner scnr = new Scanner(System.in);

            String next;
            s = new Stack<>();
            boolean exit = false;

            do {
                System.out.print("> ");
                next = scnr.next();
                switch (next) {
                    case "!":
                        exit = true;
                        break;
                    case "?":
                        printStack();
                        break;
                    case "&&":
                        intersection();
                        break;
                    case "||":
                        union();
                        break;
                    default:
                        if (hash.has(next)) {
                            s.push(next);
                            break;
                        }
                }
            } while (!exit);
        }
    }

    private static void printStack() {
        if (s.size() > 0) {
            String last = s.peek();
            for (String key : hash.get(last)) {
                System.out.println(key);
            }
        }
    }

    private static void intersection() {
        if (s.size() > 1) {
            String keyName;
            String key2 = s.pop();
            String key1 = s.pop();
            HashMap<String, Integer> newHash = new HashMap<>();
            for (String key : hash.get(key1)) {
                if (hash.get(key2).has(key)) {
                    newHash.insert(key, 1);
                }
            }
            keyName = key1 + " && " + key2;
            hash.insert(keyName, newHash);
            s.push(keyName);
        }
    }

    private static void union() {
        if (s.size() > 1) {
            String keyName;
            String key2 = s.pop();
            String key1 = s.pop();
            HashMap<String, Integer> newHash = new HashMap<>();
            for (String key : hash.get(key1)) {
                newHash.insert(key, 1);
            }
            for (String key : hash.get(key2)) {
                newHash.insert(key, 1);
            }
            keyName = key1 + " || " + key2;
            hash.insert(keyName, newHash);
            s.push(keyName);
        }
    }

    private static void loadHashMap(Scanner fs) {
        String keyLine;
        String curKey;
        String curURL = fs.nextLine();
        if (fs.hasNextLine()) {
            keyLine = fs.nextLine();
            Scanner lineSCNR = new Scanner(keyLine);
            while (lineSCNR.hasNext()) {
                curKey = lineSCNR.next();
                if (hash.has(curKey)) {
                    hash.get(curKey).insert(curURL, 1);
                } else {
                    HashMap<String, Integer> basket = new HashMap<>();
                    basket.insert(curURL, 1);
                    hash.insert(curKey, basket); //insert into HashMap
                }
            }
            lineSCNR.close();
        }
    }
}

