package hw7;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Implementation of HashMap.
 * @param <K>
 * @param <V>
 */
public class HashMap<K, V> implements Map<K, V> {
    //Graveyards will have a null value.
    private class Table {
        K key;
        V value;

        Table(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    private Table[] ray;
    private int size;
    private double loadFactor;

    /**
     * Hush CheckStyle.
     * */
    public HashMap() {
        ray = (Table[]) new HashMap.Table[11];
        size = 0;
        loadFactor = 0;
    }

    private int hash(K key, int increment) {
        int hashValue = ((key.hashCode() + increment) % ray.length);
        if (hashValue < 0) {
            hashValue = 0 - hashValue;
        }
        return hashValue;
    }

    private int find(K key) {
        //Will return index of the found element. Will return -1 if not found
        for (int i = 0; i < ray.length; i++) {
            int curHash = hash(key, i);
            if (ray[curHash] != null) {
                if (ray[curHash].key.equals(key)
                        && ray[curHash].value != null) {
                    return curHash;
                }
            } else {
                return -1;
            }
        }
        return -1;
    }

    private int openAddressing(K key) {
        // Will return index of open spot.
        // Will return -1 if key is already present
        for (int i = 0; i < ray.length; i++) {
            int curHash = hash(key, i);
            if (ray[curHash] != null && ray[curHash].key.equals(key)) {
                return -1;
            }
            if (ray[curHash] == null || ray[curHash].value == null) {
                return curHash;
            }
        }
        return -1;
    }

    private void rehash() {
        int nextSize = this.ray.length * 2 + 1;

        while (!isPrime(nextSize)) {
            nextSize += 2;
        }

        Table[] tempRay = this.ray;
        this.ray = (Table[]) new HashMap.Table[nextSize];
        for (Table t : tempRay) {
            if (t != null && t.value != null) {
                this.insert(t.key, t.value);
            }
        }

    }

    private boolean isPrime(int num) {
        int sqrt = (int) Math.sqrt(num);
        for (int i = 3; i < sqrt; i += 2) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void insert(K k, V v) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException();
        }
        if (has(k) && get(k) != null) {
            throw new IllegalArgumentException();
        }
        int index = openAddressing(k);
        if (index != -1) {
            ray[index] = new Table(k, v);
            size++;
            loadFactor = this.size / ray.length;
            if (loadFactor > .5) {
                rehash();
            }
        }
    }

    @Override
    public V remove(K k) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException();
        }
        int index = find(k);
        if (index != -1) {
            V v = ray[index].value;
            ray[index].value = null;
            size--;
            return v;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void put(K k, V v) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException();
        }
        int index = find(k);
        if (index == -1) {
            throw  new IllegalArgumentException();
        }
        ray[index].key = k;
        ray[index].value = v;
    }

    @Override
    public V get(K k) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException();
        }
        int index = find(k);
        if (index == -1) {
            throw new IllegalArgumentException();
        }
        return ray[index].value;
    }

    @Override
    public boolean has(K k) {
        if (k == null) {
            return false;
        }
        if (find(k) != -1) {
            return true;
        }
        return false;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public Iterator<K> iterator() {
        List<K> keys = new ArrayList<>();
        for (Table e: this.ray) {
            if (e != null && e.value != null) {
                keys.add(e.key);
            }
        }
        return keys.iterator();
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("{");
        for (int i = 0; i < this.ray.length; i++) {
            Table e = this.ray[i];
            if (e != null && e.value != null) {
                s.append("" + e.key + ": " + e.value);
                if (i < this.ray.length - 1) {
                    s.append(", ");
                }
            }
        }
        s.append("}");
        return s.toString();
    }
}
