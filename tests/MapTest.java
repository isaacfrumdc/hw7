package hw7.tests;

import hw7.Map;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Testing implementations of Maps.
 */
public abstract class MapTest {
    protected Map<String, String> m;

    protected abstract Map<String, String> createMap();

    @Before
    public void setupMap() {
        m = this.createMap();
    }

    @Test
    public void newMapEmpty() {
        assertEquals(0, m.size());
        assertFalse(m.has(""));

        int count = 0;
        for (String k : m) {
            count++;
        }
        assertEquals(count, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertNullKey() {
        m.insert(null, "Hello");
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeNullKey() {
        m.remove(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeKeyNotExist() {
        m.remove("Test");
    }

    @Test(expected = IllegalArgumentException.class)
    public void putNullKey() {
        m.put(null, "Test");
    }

    @Test(expected = IllegalArgumentException.class)
    public void putKeyNotExist() {
        m.put("Test", "Test");
    }

    @Test(expected = IllegalArgumentException.class)
    public void getNullKey() {
        m.get(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getKeyNotExist() {
        m.get("Test");
    }

    @Test
    public void insertSizeTest() {
        m.insert("Key", "Value");
        assertEquals(1, m.size());
        m.insert("Key2", "Value2");
        assertEquals(2, m.size());
        m.insert("Key3", "Value3");
        assertEquals(3, m.size());
    }

    @Test
    public void insertValuesTest() {
        m.insert("Key", "Value");
        m.insert("Key2", "Value2");
        m.insert("Key3", "Value3");
        assertTrue(m.has("Key"));
        assertEquals("Value", m.get("Key"));
        assertTrue(m.has("Key2"));
        assertEquals("Value2", m.get("Key2"));
        assertTrue(m.has("Key3"));
        assertEquals("Value3", m.get("Key3"));
    }

    @Test
    public void removeSizeTest() {
        m.insert("Key", "Value");
        m.insert("Key2", "Value2");
        m.remove("Key2");
        assertEquals(1, m.size());
        m.remove("Key");
        assertEquals(0, m.size());
    }

    @Test
    public void removeValuesTest() {
        m.insert("Key", "Value");
        m.insert("Key2", "Value2");
        assertEquals("Value2", m.remove("Key2"));
        assertFalse(m.has("Key2"));
        assertEquals("Value", m.remove("Key"));
        assertFalse(m.has("Key"));
    }

    @Test
    public void putTest() {
        m.insert("Key", "Value");
        m.insert("Key2", "Value2");
        m.insert("Key3", "Value3");
        m.put("Key", "Value4");
        m.put("Key2", "Value5");
        m.put("Key3", "Value6");
        assertEquals("Value4", m.get("Key"));
        assertEquals("Value5", m.get("Key2"));
        assertEquals("Value6", m.get("Key3"));
    }

    @Test
    public void testRemove() {
        m.insert("A", "Apple");
        m.remove("A");
        assertEquals(0, m.size());
        assertFalse(m.has("A"));
    }

    @Test(expected=IllegalArgumentException.class)
    public void removeUnmappedKey() {
        m.insert("A", "Apple");
        m.remove("B");
    }

    @Test
    public void testPut() {
        m.insert("A", "Apple");
        m.put("A", "Orange");
        assertEquals(1, m.size());
        assertEquals(m.get("A"), "Orange");
    }

    @Test(expected=IllegalArgumentException.class)
    public void putUnmappedKey() {
        m.insert("A", "Apple");
        m.put("B", "Orange");
    }

    @Test
    public void testGet() {
        m.insert("A", "Apple");
        assertEquals(m.get("A"), "Apple");
    }

    @Test(expected=IllegalArgumentException.class)
    public void getUnmappedKey() {
        m.insert("A", "Apple");
        m.get("B");
    }

    @Test
    public void testHas() {
        m.insert("A", "Apple");
        assertTrue(m.has("A"));
    }

    @Test
    public void testSize() {
        assertEquals(0, m.size());
        m.insert("A", "Apple");
        assertEquals(1, m.size());
        m.insert("B", "Banana");
        assertEquals(2, m.size());
    }

    @Test
    public void testInsert() {
        m.insert("A", "Apple");
        assertTrue(m.has("A"));
        assertEquals(1, m.size());
    }

    @Test(expected=IllegalArgumentException.class)
    public void insertRepeatKey() {
        m.insert("A", "Apple");
        m.insert("A", "Orange");
        System.out.println("Size:" + m.size());
        assertEquals(1, m.size());
        assertEquals(m.get("A"), "Apple");
    }


}
