package hw7.tests;

import hw7.Map;
import hw7.HashMap;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test HashMap Implementations.
 * */
public class HashMapTest extends MapTest {

    @Override
    protected Map<String, String> createMap() {
        return new HashMap<>();
    }

    @Before
    public void setupMap() {
        m = this.createMap();
    }

    @Test
    public void insertTest() {
        m.insert("abc", "a");
        m.insert("def", "a");
        m.insert("ghi", "a");
        assertFalse(m.has("123"));
        assertFalse(m.has("456"));
        assertTrue(m.has("abc"));
        assertTrue(m.has("def"));
        assertTrue(m.has("ghi"));
    }

    @Test
    public void removeTest() {
        m.insert("1", "a");
        m.insert("2", "a");
        m.insert("3", "a");
        assertFalse(m.has("4"));
        m.insert("4", "a");
        m.insert("5", "a");
        assertTrue(m.has("4"));
        m.insert("6", "a");
        m.remove("5");
        assertFalse(m.has("5"));
        assertTrue(m.has("6"));
    }

    @Test
    public void emptyMapHasNext() {
        Iterator<String> iter = m.iterator();
        assertFalse(iter.hasNext());
    }

    @Test
    public void basicOpsTest() {
        m.insert("gave", "http://alumni.jhu.edu/");
        m.insert("alumni", "http://alumni.jhu.edu/");
        m.insert("hopkins", "http://alumni.jhu.edu/");
        m.insert("association", "http://alumni.jhu.edu/");
        m.insert("give", "http://alumni.jhu.edu/");
        m.insert("johns", "http://alumni.jhu.edu/");
        m.insert("given", "http://alumni.jhu.edu/");
        assertEquals(7, m.size());
        m.remove("gave");
        m.remove("given");
        assertEquals(5, m.size());
        m.insert("given", "http://alumni.jhu.edu/");
        m.insert("gave", "http://alumni.jhu.edu/");
        assertEquals(7, m.size());
    }
}
